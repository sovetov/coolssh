import abc
import collections
import hashlib
import io
import logging
import os
import socket
import struct
from contextlib import closing, contextmanager
from typing import BinaryIO, ByteString, Iterable

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import padding, rsa
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.primitives.hashes import SHA1
from cryptography.hazmat.primitives.hmac import HMAC
from cryptography.hazmat.primitives.serialization import load_pem_private_key

_logger = logging.getLogger(__name__)

SSH_MSG_DISCONNECT = 1  # [SSH-TRANS]
SSH_MSG_IGNORE = 2  # [SSH-TRANS]
SSH_MSG_UNIMPLEMENTED = 3  # [SSH-TRANS]
SSH_MSG_DEBUG = 4  # [SSH-TRANS]
SSH_MSG_SERVICE_REQUEST = 5  # [SSH-TRANS]
SSH_MSG_SERVICE_ACCEPT = 6  # [SSH-TRANS]
SSH_MSG_KEXINIT = 20  # [SSH-TRANS]
SSH_MSG_NEWKEYS = 21  # [SSH-TRANS]
SSH_MSG_KEXDH_INIT = 30  # Couldn't find in the newest version!
SSH_MSG_KEXDH_REPLY = 31  # Couldn't find in the newest version!
SSH_MSG_USERAUTH_REQUEST = 50  # [SSH-USERAUTH]
SSH_MSG_USERAUTH_FAILURE = 51  # [SSH-USERAUTH]
SSH_MSG_USERAUTH_SUCCESS = 52  # [SSH-USERAUTH]
SSH_MSG_USERAUTH_BANNER = 53  # [SSH-USERAUTH]
SSH_MSG_USERAUTH_PK_OK = 60  # RFC 4252, method-specific message number.
SSH_MSG_GLOBAL_REQUEST = 80  # [SSH-CONNECT]
SSH_MSG_REQUEST_SUCCESS = 81  # [SSH-CONNECT]
SSH_MSG_REQUEST_FAILURE = 82  # [SSH-CONNECT]
SSH_MSG_CHANNEL_OPEN = 90  # [SSH-CONNECT]
SSH_MSG_CHANNEL_OPEN_CONFIRMATION = 91  # [SSH-CONNECT]
SSH_MSG_CHANNEL_OPEN_FAILURE = 92  # [SSH-CONNECT]
SSH_MSG_CHANNEL_WINDOW_ADJUST = 93  # [SSH-CONNECT]
SSH_MSG_CHANNEL_DATA = 94  # [SSH-CONNECT]
SSH_MSG_CHANNEL_EXTENDED_DATA = 95  # [SSH-CONNECT]
SSH_MSG_CHANNEL_EOF = 96  # [SSH-CONNECT]
SSH_MSG_CHANNEL_CLOSE = 97  # [SSH-CONNECT]
SSH_MSG_CHANNEL_REQUEST = 98  # [SSH-CONNECT]
SSH_MSG_CHANNEL_SUCCESS = 99  # [SSH-CONNECT]
SSH_MSG_CHANNEL_FAILURE = 100  # [SSH-CONNECT]


def read_byte(stream):
    return stream.read(1)[0]


def read_boolean(stream):
    return stream.read(1) != b'\0'


def read_bytes(stream, size):
    return stream.read(size)


def read_uint32(stream):
    packed = stream.read(4)
    if not packed:
        raise RuntimeError("Stream is closed by other party")
    [unpacked] = struct.unpack('!I', packed)
    assert isinstance(unpacked, int)
    return unpacked


def read_uint64(stream):
    packed = stream.read(8)
    [unpacked] = struct.unpack('!Q', packed)
    assert isinstance(unpacked, int)
    return unpacked


def read_string(stream):
    """Read arbitrary length binary string"""
    length = read_uint32(stream)
    return stream.read(length)


def read_name_list(stream):
    joined = read_string(stream)
    if len(joined) == 0:
        return []
    encoded = joined.split(b',')
    decoded = [item.decode('ascii') for item in encoded]
    return decoded


def read_mpint(stream):
    """Read multi precision int"""
    string = read_string(stream)
    value = int.from_bytes(string, 'big', signed=True)
    return value


def encode_byte(value: int):
    return bytearray([value])


def encode_boolean(value: bool):
    return b'\1' if value else b'\0'


def encode_bytes(value: bytes):
    return value


def encode_uint32(value: int):
    return struct.pack('!I', value)


def encode_uint64(value: int):
    return struct.pack('!Q', value)


def encode_string(value):
    return encode_uint32(len(value)) + value


def encode_name_list(value: Iterable[str]):
    encoded_names = []
    for name in value:
        if ',' in name:
            raise ValueError("Names in name list cannot contain comma (,)")
        encoded_name = name.encode('ascii')
        encoded_names.append(encoded_name)
    return encode_string(b','.join(encoded_names))


def encode_mpint(value: int):
    """Write multi precision int"""
    if value == 0:
        return b''
    # TODO: Use cryptography.utils.int_to_bytes.
    length = value.bit_length() // 8 + 1  # Simplified to this.
    string = value.to_bytes(length, 'big', signed=True)
    return encode_string(string)


class PacketsIn(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def read_packet(self):
        pass


class PacketsOut(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def write_packet(self, payload):
        pass


class RawPacketsIn(PacketsIn):
    def __init__(self, in_data, in_count):
        self._in_data = in_data
        self._block_len = 8
        self._read_counter = in_count

    def read_packet(self):
        packet_len = read_uint32(self._in_data)
        padding_len = read_byte(self._in_data)
        payload = read_bytes(self._in_data, packet_len - padding_len - 1)
        _random_padding = read_bytes(self._in_data, padding_len)
        self._read_counter.inc()
        return payload


def _pad(payload_len, block_len, min_packet_len):
    assert min_packet_len % block_len == 0
    unpadded_len = payload_len + 4 + 1
    if unpadded_len < min_packet_len:
        padding_len = min_packet_len - unpadded_len
    elif unpadded_len % block_len != 0:
        padding_len = block_len - unpadded_len % block_len
    else:
        padding_len = 0
    if padding_len < 4:
        padding_len += block_len
    assert 4 <= padding_len <= 255
    packet_len = 1 + payload_len + padding_len
    assert 4 + packet_len >= min_packet_len
    assert (4 + packet_len) % block_len == 0
    return packet_len, padding_len


class RawPacketsOut(PacketsOut):
    def __init__(self, out_stream, out_count):
        self._write_counter = out_count
        self._out_stream = out_stream
        self._block_len = 8

    def write_packet(self, payload):
        min_packet_len = max(16, self._block_len)
        block_len = max(8, self._block_len)
        packet_len, padding_len = _pad(len(payload), block_len, min_packet_len)
        self._out_stream.write(encode_uint32(packet_len))
        self._out_stream.write(encode_byte(len(b'\0' * padding_len)))
        self._out_stream.write(encode_bytes(payload))
        self._out_stream.write(encode_bytes(b'\0' * padding_len))
        self._out_stream.flush()
        self._write_counter.inc()


def read_kex(stream):
    kex = {
        'cookie': read_bytes(stream, 16),
        'kex_algorithms': read_name_list(stream),
        'server_host_key_algorithms': read_name_list(stream),
        'encryption_algorithms_client_to_server': read_name_list(stream),
        'encryption_algorithms_server_to_client': read_name_list(stream),
        'mac_algorithms_client_to_server': read_name_list(stream),
        'mac_algorithms_server_to_client': read_name_list(stream),
        'compression_algorithms_client_to_server': read_name_list(stream),
        'compression_algorithms_server_to_client': read_name_list(stream),
        'languages_client_to_server': read_name_list(stream),
        'languages_server_to_client': read_name_list(stream),
        'first_kex_packet_follows': read_boolean(stream),
        }
    assert read_uint32(stream) == 0
    return kex


_client_kex = {
    'cookie': os.urandom(16),
    'kex_algorithms': ['diffie-hellman-group14-sha1'],
    'server_host_key_algorithms': ['ssh-rsa'],
    'encryption_algorithms_client_to_server': ['aes128-ctr'],
    'encryption_algorithms_server_to_client': ['aes128-ctr'],
    'mac_algorithms_client_to_server': ['hmac-sha1'],
    'mac_algorithms_server_to_client': ['hmac-sha1'],
    'compression_algorithms_client_to_server': ['none'],
    'compression_algorithms_server_to_client': ['none'],
    'languages_client_to_server': [],
    'languages_server_to_client': [],
    'first_kex_packet_follows': False,
    }


class KeyMaker:
    hash_algo = hashlib.sha1

    def __init__(self, shared_secret, exchange_hash_data, session_id):
        self._initial_hash_obj = self.hash_algo()
        self._initial_hash_obj.update(encode_mpint(shared_secret))
        self._initial_hash_obj.update(encode_bytes(exchange_hash_data))  # Bytes, not mentioned in RFC!
        self._session_id = session_id

    def make_key(self, letter):
        hash_obj = self._initial_hash_obj.copy()
        hash_obj.update(letter.encode('ascii'))
        hash_obj.update(self._session_id)
        part = hash_obj.digest()
        key = bytearray(part)
        while len(key) < 8192 // 8:
            hash_obj.update(part)  # TODO: How encoded?
            part = hash_obj.digest()  # Yes, rewrite.
            key.extend(part)
        return key


class IntegrityAlgo:
    def __init__(self, hash_algo, key_len, digest_len):
        self.key_len = key_len
        self.hash_algo = hash_algo
        self.digest_len = digest_len

    def get_checker(self, key):
        checker = IntegrityChecker(self, key)
        return checker


class IntegrityChecker:
    def __init__(self, algo, key):
        self._algo = algo
        self._key = key[:algo.key_len]
        self.digest_len = algo.digest_len

    def make_context(self):
        context = HMAC(self._key, self._algo.hash_algo, default_backend())
        return context


hmac_sha1 = IntegrityAlgo(SHA1(), key_len=20, digest_len=20)


class SymmetricAlgo:
    def __init__(self, algo, mode, block_len):
        self._algo = algo
        self.block_len = block_len
        self._mode = mode

    def make_context(self, key, iv):
        algo_initialized = self._algo(key[:self.block_len])
        mode_initialized = self._mode(iv[:self.block_len])
        cipher = Cipher(algo_initialized, mode_initialized, default_backend())
        context = SymmetricContext(self.block_len, cipher)
        return context


class SymmetricContext:
    def __init__(self, block_len, cipher):
        self.block_len = block_len
        self._cipher = cipher

    def encryption_context(self):
        return self._cipher.encryptor()

    def decryption_context(self):
        return self._cipher.decryptor()


aes128_ctr = SymmetricAlgo(algorithms.AES, modes.CTR, block_len=128 // 8)


class Counter:
    def __init__(self):
        self._value = 0

    def inc(self):
        self._value += 1
        self._value %= 2 ** 32

    def get(self):
        return self._value


class _EncryptedStream:
    def __init__(self, stream, cipher_block_len, integrity_checker, counter):
        self.counter = counter
        self._stream = stream
        self._block_len = max(8, cipher_block_len)
        self._integrity_checker = integrity_checker


class EncryptedPacketsIn(PacketsIn, _EncryptedStream):
    def __init__(self, stream, symmetric_context, integrity_checker, counter):
        super().__init__(stream, symmetric_context.block_len, integrity_checker, counter)
        self._decryption_context = symmetric_context.decryption_context()

    def read_packet(self):
        unencrypted_packet = bytearray()
        while len(unencrypted_packet) < 4:
            block = self._stream.read(self._block_len)
            if not block:
                raise RuntimeError("Server closed connection")
            unencrypted_packet.extend(self._decryption_context.update(block))
        packet_len = int.from_bytes(unencrypted_packet[:4], 'big')
        if (packet_len + 4) % self._block_len != 0:
            raise RuntimeError("Packet len + 4 must be multiple of bock size")
        while packet_len + 4 > len(unencrypted_packet):
            unencrypted_packet.extend(self._decryption_context.update(self._stream.read(self._block_len)))
        assert len(unencrypted_packet) == packet_len + 4
        integrity_context = self._integrity_checker.make_context()
        integrity_context.update(self.counter.get().to_bytes(4, 'big'))
        integrity_context.update(unencrypted_packet)
        integrity_len = integrity_context.algorithm.digest_size
        received_mac = self._stream.read(integrity_len)
        calculated_mac = integrity_context.finalize()[:self._integrity_checker.digest_len]
        if received_mac != calculated_mac:
            raise RuntimeError("MAC doesn't match")
        data_stream = io.BytesIO(unencrypted_packet)
        _packet_len = read_uint32(data_stream)
        padding_len = read_byte(data_stream)
        payload = read_bytes(data_stream, packet_len - 1 - padding_len)
        self.counter.inc()
        return payload


class EncryptedPacketsOut(PacketsOut, _EncryptedStream):
    def __init__(self, stream, symmetric_context, integrity_checker, counter):
        super().__init__(stream, symmetric_context.block_len, integrity_checker, counter)
        self._encryption_context = symmetric_context.encryption_context()

    def write_packet(self, payload):
        packet_len, padding_len = _pad(len(payload), self._block_len, min(16, self._block_len))
        unencrypted_packet = b''.join((
            encode_uint32(packet_len),
            encode_byte(padding_len),
            encode_bytes(payload),
            encode_bytes(b'\xFF' * padding_len),
            ))
        assert len(unencrypted_packet) == packet_len + 4
        encrypted_packet = self._encryption_context.update(unencrypted_packet)
        assert len(encrypted_packet) == len(unencrypted_packet)
        self._stream.write(encrypted_packet)
        integrity_context = self._integrity_checker.make_context()
        integrity_context.update(self.counter.get().to_bytes(4, 'big'))
        integrity_context.update(unencrypted_packet)
        mac = integrity_context.finalize()[:self._integrity_checker.digest_len]
        self._stream.write(mac)
        self._stream.flush()
        self.counter.inc()


def _request_service(service_name, packets_in: PacketsIn, packets_out: PacketsOut):
    packets_out.write_packet(encode_byte(SSH_MSG_SERVICE_REQUEST) + encode_string(service_name))
    _logger.info("Expect SSH_MSG_SERVICE_ACCEPT of ssh-userauth")
    in_data_stream = io.BytesIO(packets_in.read_packet())
    in_packet_type = read_byte(in_data_stream)
    if in_packet_type != SSH_MSG_SERVICE_ACCEPT:
        raise RuntimeError("Expected SSH_MSG_SERVICE_ACCEPT but got {}".format(in_packet_type))
    in_service = read_string(in_data_stream)
    if in_service != service_name:
        raise RuntimeError("Expected ssh-userauth but got {}".format(in_service))


class _SshRsa:
    type = b'ssh-rsa'

    def __init__(self, impl):
        self._private_key = impl  # type: rsa.RSAPrivateKey
        self.public_blob = b''.join((
            encode_string(b'ssh-rsa'),
            encode_mpint(self._private_key.public_key().public_numbers().e),
            encode_mpint(self._private_key.public_key().public_numbers().n),
            ))

    @classmethod
    def from_file(cls, path):
        with open(str(path), 'rb') as f:
            formatted = f.read()
        private_key = load_pem_private_key(formatted, None, default_backend())
        return cls(private_key)

    def sign(self, data):
        """RFC 4253, paragraph 6.6"""
        raw_signature = self._private_key.sign(data, padding.PKCS1v15(), SHA1())
        return encode_string(self.type) + encode_string(raw_signature)


class _PublicKeyMethod:
    _name = b'publickey'

    def __init__(self, username, private_key):
        self._username = username
        self._private_key = private_key

    def _rsa_auth_packet(self, offer_only=False, session_id=None):
        data = b''.join((
            encode_byte(SSH_MSG_USERAUTH_REQUEST),
            encode_string(self._username.encode('utf8')),
            encode_string(b'ssh-connection'),
            encode_string(self._name),
            encode_boolean(not offer_only),
            encode_string(self._private_key.type),
            encode_string(self._private_key.public_blob),
            ))
        if offer_only:
            return data
        signed_data = encode_string(session_id) + encode_bytes(data)
        signature = self._private_key.sign(signed_data)
        return data + encode_string(signature)

    def _offer(self, packets_in: PacketsIn, packets_out: PacketsOut):
        payload = self._rsa_auth_packet(offer_only=True)
        packets_out.write_packet(payload)

        data_stream = io.BytesIO(packets_in.read_packet())
        message_number = read_byte(data_stream)
        if message_number == SSH_MSG_USERAUTH_FAILURE:
            raise RuntimeError("Auth failed")
        if message_number != SSH_MSG_USERAUTH_PK_OK:
            raise RuntimeError("Got unexpected message")
        if read_string(data_stream) != self._private_key.type:
            raise RuntimeError("Got unexpected auth key type")
        if read_string(data_stream) != self._private_key.public_blob:
            raise RuntimeError("Got unexpected public blob")

    def _authenticate(self, packets_in, packets_out, session_id):
        payload = self._rsa_auth_packet(session_id=session_id)
        packets_out.write_packet(payload)

        data_stream = io.BytesIO(packets_in.read_packet())
        message_number = read_byte(data_stream)
        if message_number == SSH_MSG_USERAUTH_FAILURE:
            raise RuntimeError("Auth failed")
        if message_number != SSH_MSG_USERAUTH_SUCCESS:
            raise RuntimeError("Got unexpected message")

    def perform(self, packets_in, packets_out, session_id):
        _request_service(b'ssh-userauth', packets_in, packets_out)
        self._offer(packets_in, packets_out)
        self._authenticate(packets_in, packets_out, session_id=session_id)


class Connection:
    def __init__(self, binary_stream, client_id='SSH-2.0-coolssh'):
        self.binary_stream = binary_stream
        self._client_id = client_id.encode('ascii') + b'\n'
        self._perform_identification()

        # Packet numbers are used in each message.
        # Incoming and outgoing packet numbers are independent.
        self._read_counter = Counter()
        self._write_counter = Counter()

        # Below, message streams will be independently ciphered.
        # Hence the notion of duplex.
        self._packets_in = RawPacketsIn(binary_stream, self._read_counter)
        self._packets_out = RawPacketsOut(binary_stream, self._write_counter)

        self._session_id = None
        self._kex_init()
        self._kex_dh()
        self._use_new_keys()

        self._cache = {}

        private_key = _SshRsa.from_file('/home/gsovetov/.ssh/id_rsa')
        method = _PublicKeyMethod('gsovetov', private_key)
        method.perform(self._packets_in, self._packets_out, self._session_id)

        self._channels_opened = 0

    def _perform_identification(self):
        logger = _logger.getChild('id')
        logger.info("Send id string: %r", self._client_id)
        self.binary_stream.write(self._client_id)
        self.binary_stream.flush()
        while True:
            line = self.binary_stream.readline(255)  # Limit by RFC-4253.
            if line.startswith(b'SSH-'):
                logger.info("Received id string: %r", line)
                self.server_id = line
                version, has_comments, comments = self.server_id.partition(b' ')
                if has_comments:
                    logger.info("Received comments: %r", comments)
                _ssh, proto_version, software_version = version.split(b'-', 2)
                logger.info("Received proto version: %r", proto_version)
                logger.info("Received software version: %r", software_version)
                break
            logger.info("Received other line: %r", line)

    def _kex_init(self):
        kex = _client_kex
        self.client_kex_payload = b''.join((  # For hashes.
            encode_byte(SSH_MSG_KEXINIT),
            encode_bytes(kex['cookie']),
            encode_name_list(kex['kex_algorithms']),
            encode_name_list(kex['server_host_key_algorithms']),
            encode_name_list(kex['encryption_algorithms_client_to_server']),
            encode_name_list(kex['encryption_algorithms_server_to_client']),
            encode_name_list(kex['mac_algorithms_client_to_server']),
            encode_name_list(kex['mac_algorithms_server_to_client']),
            encode_name_list(kex['compression_algorithms_client_to_server']),
            encode_name_list(kex['compression_algorithms_server_to_client']),
            encode_name_list(kex['languages_client_to_server']),
            encode_name_list(kex['languages_server_to_client']),
            encode_boolean(kex['first_kex_packet_follows']),
            encode_uint32(0),
            ))
        self._packets_out.write_packet(self.client_kex_payload)
        self.server_kex_payload = self._packets_in.read_packet()  # For hashes.
        server_kex_data = io.BytesIO(self.server_kex_payload)
        assert read_byte(server_kex_data) == SSH_MSG_KEXINIT
        _server_kex = read_kex(server_kex_data)

    def _kex_dh(self):
        """4253, section 8; RFC 3526, section 3"""
        order = 2048
        prime_copied = '''
            FFFFFFFF FFFFFFFF C90FDAA2 2168C234 C4C6628B 80DC1CD1
            29024E08 8A67CC74 020BBEA6 3B139B22 514A0879 8E3404DD
            EF9519B3 CD3A431B 302B0A6D F25F1437 4FE1356D 6D51C245
            E485B576 625E7EC6 F44C42E9 A637ED6B 0BFF5CB6 F406B7ED
            EE386BFB 5A899FA5 AE9F2411 7C4B1FE6 49286651 ECE45B3D
            C2007CB8 A163BF05 98DA4836 1C55D39A 69163FA8 FD24CF5F
            83655D23 DCA3AD96 1C62F356 208552BB 9ED52907 7096966D
            670C354E 4ABC9804 F1746C08 CA18217C 32905E46 2E36CE3B
            E39E772C 180E8603 9B2783A2 EC07A28F B5C55DF0 6F4C52C9
            DE2BCBF6 95581718 3995497C EA956AE5 15D22618 98FA0510
            15728E5A 8AACAA68 FFFFFFFF FFFFFFFF
        '''
        prime_str = prime_copied.replace(' ', '').replace('\n', '')
        prime_bytes = bytes.fromhex(prime_str)
        prime = int.from_bytes(prime_bytes, 'big')  # First byte is most significant.
        generator = 2
        x = int.from_bytes(os.urandom(order // 8), 'little')  # Byte order doesn't matter.
        e = pow(generator, x, prime)

        self._packets_out.write_packet(encode_byte(SSH_MSG_KEXDH_INIT) + encode_mpint(e))

        reply_data = io.BytesIO(self._packets_in.read_packet())
        assert read_byte(reply_data) == SSH_MSG_KEXDH_REPLY
        host_key_payload = read_string(reply_data)
        f = read_mpint(reply_data)
        signature_payload = read_string(reply_data)
        del reply_data

        self.shared_secret = pow(f, x, prime)

        host_key_data = io.BytesIO(host_key_payload)
        host_key_algorithm = read_string(host_key_data)
        assert host_key_algorithm == b'ssh-rsa'
        public_exponent = read_mpint(host_key_data)
        modulus = read_mpint(host_key_data)
        host_key = rsa.RSAPublicNumbers(public_exponent, modulus).public_key(default_backend())
        del host_key_data, host_key_algorithm

        hashed = b''.join((
            encode_string(self._client_id.rstrip()),
            encode_string(self.server_id.rstrip()),
            encode_string(self.client_kex_payload),
            encode_string(self.server_kex_payload),
            encode_string(host_key_payload),
            encode_mpint(e),
            encode_mpint(f),
            encode_mpint(self.shared_secret),
            ))
        self.exchange_hash = hashlib.sha1(hashed).digest()

        if self._session_id is None:
            self._session_id = self.exchange_hash

        signature_data = io.BytesIO(signature_payload)
        signature_algorithm = read_string(signature_data)
        assert signature_algorithm == b'ssh-rsa'
        signature = read_string(signature_data)
        host_key.verify(signature, self.exchange_hash, padding.PKCS1v15(), SHA1())

    def _use_new_keys(self):
        # Server to Client or Client to Server.
        key_maker = KeyMaker(self.shared_secret, self.exchange_hash, self._session_id)
        initial_iv_cts = key_maker.make_key('A')
        initial_iv_stc = key_maker.make_key('B')
        encryption_key_cts = key_maker.make_key('C')
        encryption_key_stc = key_maker.make_key('D')
        integrity_key_cts = key_maker.make_key('E')
        integrity_key_stc = key_maker.make_key('F')
        del key_maker
        self._packets_out.write_packet(encode_byte(SSH_MSG_NEWKEYS))
        self._packets_out = EncryptedPacketsOut(
            self.binary_stream,
            aes128_ctr.make_context(encryption_key_cts, initial_iv_cts),
            hmac_sha1.get_checker(integrity_key_cts),
            self._write_counter)
        server_new_keys_packet = self._packets_in.read_packet()
        server_new_keys_data = io.BytesIO(server_new_keys_packet)
        if read_byte(server_new_keys_data) != SSH_MSG_NEWKEYS:
            raise RuntimeError("Expected NEWKEYS from server")
        self._packets_in = EncryptedPacketsIn(
            self.binary_stream,
            aes128_ctr.make_context(encryption_key_stc, initial_iv_stc),
            hmac_sha1.get_checker(integrity_key_stc),
            self._read_counter)

    def _process_global_request(self, packet_type, packet):
        if packet_type == SSH_MSG_DISCONNECT:
            reason_code = read_uint32(packet)
            description = read_string(packet).decode('UTF-8')
            _language = read_string(packet)
            raise RuntimeError(
                "Disconnect request received: {} {}".format(
                    reason_code, description))
        if packet_type == SSH_MSG_GLOBAL_REQUEST:
            request_name = read_string(packet)
            want_reply = read_boolean(packet)
            if b'@' in request_name:
                _logger.warning("Unsupported custom request: %s", request_name)
                if want_reply:
                    self._packets_out.write_packet(encode_byte(SSH_MSG_REQUEST_FAILURE))
            else:
                raise RuntimeError("Unsupported global request: {}".format(request_name))

    def _receive_one(self):
        data_in = io.BytesIO(self._packets_in.read_packet())
        packet_type = read_byte(data_in)
        if not 90 <= packet_type <= 100:
            self._process_global_request(packet_type, data_in)
        chan_id = read_uint32(data_in)
        try:
            cache = self._cache[chan_id]
        except KeyError:
            cache = self._cache[chan_id] = collections.deque()
        cache.append((packet_type, data_in))

    def read_packet(self, chan_id):
        while True:
            try:
                return self._cache[chan_id].popleft()
            except LookupError:  # Expect from dict and deque.
                pass
            self._receive_one()

    def Popen(self, command):
        chan_id = self._channels_opened
        self._channels_opened += 1
        return _Popen(self, self._packets_out, chan_id, command)


_channel_open_failure_reasons = {
    1: 'SSH_OPEN_ADMINISTRATIVELY_PROHIBITED',
    2: 'SSH_OPEN_CONNECT_FAILED',
    3: 'SSH_OPEN_UNKNOWN_CHANNEL_TYPE',
    4: 'SSH_OPEN_RESOURCE_SHORTAGE',
    }


class _Popen:
    def __init__(self, connection: Connection, packets_out: PacketsOut, chan_id: int, command: ByteString):
        self._conn = connection
        self._packets_out = packets_out
        self._chan_id = chan_id
        self._command = command
        self._logger = _logger.getChild('chan').getChild(str(chan_id))
        self._open_session()
        self._exec()

    def _open_session(self):
        self._logger.info("Open: attempt")
        self._packets_out.write_packet(b''.join((
            encode_byte(SSH_MSG_CHANNEL_OPEN),
            encode_string(b'session'),
            encode_uint32(self._chan_id),
            encode_uint32(2 ** 32 - 1),  # Init window size.
            encode_uint32(2 ** 32 - 1),  # Max packet size.
            )))
        packet_type, data_in = self._conn.read_packet(self._chan_id)
        if packet_type == SSH_MSG_CHANNEL_OPEN_CONFIRMATION:
            self._logger.debug("Open: success")
            self._their_chan_id = read_uint32(data_in)
            self._their_window = read_uint32(data_in)
            _their_max_size = read_uint32(data_in)
            return
        if packet_type == SSH_MSG_CHANNEL_OPEN_FAILURE:
            reason_code = read_uint32(data_in)
            reason = _channel_open_failure_reasons.get(reason_code, reason_code)
            description = read_string(data_in).decode('utf8')  # RFC 4254 5.1.
            language = read_string(data_in).decode('ascii')  # RFC 3066 2.1.
            raise RuntimeError("Open: fail: %s %s %s", reason, description, language)
        raise RuntimeError("Unexpected message type: {}".format(packet_type))

    def _exec(self):
        self._logger.debug("Exec: %r", self._command)
        self._packets_out.write_packet(b''.join((
            encode_byte(SSH_MSG_CHANNEL_REQUEST),
            encode_uint32(self._their_chan_id),
            encode_string(b'exec'),
            encode_boolean(True),  # Want reply.
            encode_string(self._command),
            )))
        while True:
            packet_type, data_in = self._conn.read_packet(self._chan_id)
            if packet_type == SSH_MSG_CHANNEL_WINDOW_ADJUST:
                self._their_window += read_uint32(data_in)
                continue
            if packet_type == SSH_MSG_CHANNEL_SUCCESS:
                self._logger.debug("Exec: success")
                break
            if packet_type == SSH_MSG_CHANNEL_FAILURE:
                raise RuntimeError("Exec: fail")
            raise RuntimeError("Unexpected message type: {}".format(packet_type))

    def communicate(self):
        state = 'open'
        while True:
            packet_type, data_in = self._conn.read_packet(self._chan_id)
            if packet_type == SSH_MSG_CHANNEL_WINDOW_ADJUST:
                self._their_window += read_uint32(data_in)
                continue
            if packet_type == SSH_MSG_CHANNEL_EXTENDED_DATA:
                assert state == 'open'
                assert read_uint32(data_in) == 1  # Data type code for stderr.
                data_in = read_string(data_in)
                self._logger.info("STDERR: %r", data_in)
                continue
            if packet_type == SSH_MSG_CHANNEL_DATA:
                assert state == 'open'
                data_in = read_string(data_in)
                self._logger.info("STDOUT: %r", data_in)
                continue
            if packet_type == SSH_MSG_CHANNEL_EOF:
                state = 'eof'
                self._logger.info("EOF")
                continue
            if packet_type == SSH_MSG_CHANNEL_REQUEST:
                request_name = read_string(data_in).decode('ascii')
                want_reply = read_boolean(data_in)  # Must be FALSE.
                assert not want_reply
                if request_name == 'exit-status':
                    exit_status = read_uint32(data_in)
                    self._logger.info("EXIT STATUS: %d", exit_status)
                    continue
                raise RuntimeError("Unexpected request: {}".format(request_name))
            if packet_type == SSH_MSG_CHANNEL_CLOSE:
                assert state == 'open' or state == 'eof'
                state = 'closed'
                break
            raise RuntimeError("Unexpected message type: {}".format(packet_type))


@contextmanager
def connected(hostname, port=22):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    with closing(sock):
        sock.connect((hostname, port))
        binary_stream = sock.makefile(mode='rwb')  # type: BinaryIO
        del sock  # Will work with buffered file, not socket.
        yield binary_stream


def main():
    with connected('127.0.0.1', 10022) as binary_stream:
        connection = Connection(binary_stream)
        runs = []
        for i in range(10):
            command = 'echo {}'.format(i).encode('ascii')
            runs.append(connection.Popen(command))
        for run in runs:
            run.communicate()


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    logging.info("Run: while sudo /usr/sbin/sshd -p10022 -d || :; do :; done")
    main()
