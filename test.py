import abc
import io
import unittest

import coolssh_


class DataStreamTestCase(abc.ABC, unittest.TestCase):

    @abc.abstractmethod
    def read(self, data_stream: coolssh_.DataStream):
        pass

    def assertWrite(self, unpacked, expected_packed_hex):
        raw = io.BytesIO()
        data_stream = coolssh_.DataStream(raw)
        self.write(data_stream, unpacked)
        expected_bytes = bytes.fromhex(expected_packed_hex)
        actual_bytes = raw.getvalue()
        self.assertEqual(actual_bytes, expected_bytes)

    @abc.abstractmethod
    def write(self, data_stream: coolssh_.DataStream, value):
        pass

    def assertRead(self, packed_hex, expected_unpacked):
        packed_bytes = bytes.fromhex(packed_hex)
        raw = io.BytesIO(packed_bytes)
        data_stream = coolssh_.DataStream(raw)
        actual_unpacked = self.read(data_stream)
        self.assertEqual(actual_unpacked, expected_unpacked)


class NameListTestCase(DataStreamTestCase):
    def read(self, data_stream: coolssh_.DataStream):
        return data_stream.read_name_list()

    def write(self, data_stream: coolssh_.DataStream, value):
        data_stream.write_name_list(value)

    def testRead(self):
        self.assertRead('00 00 00 00', [])
        self.assertRead('00 00 00 04 7a 6c 69 62', ['zlib'])
        self.assertRead('00 00 00 09 7a 6c 69 62 2c 6e 6f 6e 65', ['zlib', 'none'])

    def testWrite(self):
        self.assertWrite([], '00 00 00 00')
        self.assertWrite(['zlib'], '00 00 00 04 7a 6c 69 62')
        self.assertWrite(['zlib', 'none'], '00 00 00 09 7a 6c 69 62 2c 6e 6f 6e 65')


class MPIntTestCase(DataStreamTestCase):

    def read(self, data_stream: coolssh_.DataStream):
        return data_stream.read_mpint()

    def write(self, data_stream: coolssh_.DataStream, value):
        data_stream.write_mpint(value)

    def testRead(self):
        self.assertRead('00 00 00 00', 0)
        self.assertRead('00 00 00 08 09 a3 78 f9 b2 e3 32 a7', 0x9a378f9b2e332a7)
        self.assertRead('00 00 00 02 00 80', 0x80)
        self.assertRead('00 00 00 02 ed cc', -0x1234)
        self.assertRead('00 00 00 05 ff 21 52 41 11', -0xdeadbeef)

    def testWrite(self):
        self.assertWrite(0, '00 00 00 00')
        self.assertWrite(0x9a378f9b2e332a7, '00 00 00 08 09 a3 78 f9 b2 e3 32 a7')
        self.assertWrite(0x80, '00 00 00 02 00 80')
        self.assertWrite(-0x1234, '00 00 00 02 ed cc')
        self.assertWrite(-0xdeadbeef, '00 00 00 05 ff 21 52 41 11')
